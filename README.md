# Spike Report

## SPIKE 1 - Setting Up Your Development Enviorment

### Introduction

This class uses Unreal Engine, C++, and git (using SourceTree, and the GitFlow method). 
Therefore, it is required that we know how to set up the environment to support later work.

### Goals

1.	Set up a git repository with a proper gitignore file.
2.	Setup a Unreal engine First person shooter C++ project, without Starter Materials and commit to git.


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* http://rancoud.com/gitignore-unreal-engine/ link to .gitignore that mine is based on.
* Source Tree 
* Bitbucket
* Unreal Engine

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Go To bitbucket.org sign in
    1. Once singed in look for plus sign and create repo.
    1. Give Repo a name relevent to the project. Make sure Repo type is Git.
    1. Create repository.
2. After the repo loads, Create a .gitignore file 
    1. add folders, files and filetypes to the .gitignore file for Unreal Project.
3. In SourceTree version 2.1.2.5+ go to the Remote tab.
    3. Find the relevent Repository
    3. Set the Path to where you are going to put the Unreal Project
    3. Clone Repo.
4. Created the Unreal Project
    4. Launch the Unreal Editor from the Epic Games Launcher
    4. Select the New Project Tab
    4. than Go to the C++ Tab. Select the First Person template.
    4. At the bottum of the Unreal Project Browser set the Folder to the Folder that contains the .git folder.
    4. Give Project Name than create project
5. Test The gitignore works
    5. Move the gitignore into the Unreal project folder.
    5. Stage the .gitignores and commit them. Push to remote
    5. Make sure none of the files or folders in the gitignore appear in the unstaged files area for example the intermedite folder.
6. Stage the files to commit and push to the server.
7. Setup Git Flow
    7. In Source Tree press Git Flow button 
    7. Use Default Values.
    7. Commit and push all branches.

### What we found out

This Spike breifly covers how to create a Unreal Project and the basic folder structure

We Found out how to create a C++ project ussing a template allowing the use of C++ in the project.

We Found out how to create a unreal project that is source controlled via git, Specifically how to set up the git workflow. Allowing for a workflow that has a main branch that can be used for stable builds or release and a development branch.

We Found out how to use GitFlow: Gitflow is a workflow that creates a develop branch, release branch(main) , feature branch and a hotfix branch. The Feature branch is used to create new features and once the feture is complete and when tested is pushed to the develop branch. For More about this workflow [click here](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow)


### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.