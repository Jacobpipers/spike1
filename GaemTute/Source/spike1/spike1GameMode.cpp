// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spike1.h"
#include "spike1GameMode.h"
#include "spike1HUD.h"
#include "spike1Character.h"

Aspike1GameMode::Aspike1GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aspike1HUD::StaticClass();
}
