// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "spike1GameMode.generated.h"

UCLASS(minimalapi)
class Aspike1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aspike1GameMode();
};



