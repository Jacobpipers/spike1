// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "spike1HUD.generated.h"

UCLASS()
class Aspike1HUD : public AHUD
{
	GENERATED_BODY()

public:
	Aspike1HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

